/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 15:04:59 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/01 17:54:37 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int result;
	int i;

	if (nb < 0)
		return (0);
	if (nb > 12)
		return (0);
	i = 1;
	result = i;
	while (i <= nb)
	{
		result = result * i;
		i++;
	}
	return (result);
}
