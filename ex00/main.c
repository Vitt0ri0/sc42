#include "ft_iterative_factorial.c"
#include <stdio.h>

void test(int a) {
	int res;
	res = ft_iterative_factorial(a);
	printf("%d: %d\n", a, res);
}

int main() {
	int i = -5;
	while (i < 20) {
		test(i);
		i++;
	}
}
