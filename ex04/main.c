#include <stdio.h>

int ft_fibonacci(int index);

void test(int i, int expect) {
	int res;
	res = ft_fibonacci(i);

	char stat;
	if (res == expect) {
		stat = '.';
	}
	else {
		stat = 'x';
	}

	printf("%c%c%c : index: %d, result: %d, expect: %d\n", stat, stat, stat, i, res, expect);
}

int main() {
	test(-1, -1);
	test(-10, -1);
	test(0, 0);
	test(1, 1);
	test(2, 1);
	test(3, 2);
	test(4, 3);
	test(5, 5);
	test(6, 8);
	test(7, 13);
}
