#include <stdio.h>

int ft_recursive_power(int nb, int power);

void test(int nb, int power, int expect) {
	int res;
	res = ft_recursive_power(nb, power);

	char stat;
	if (res == expect) {
		stat = '.';
	}
	else {
		stat = 'x';
	}

	printf("%c%c%c : number: %d, power: %d, result: %d, expect: %d\n", stat, stat, stat, nb, power, res, expect);
}

int main() {
	printf("%s", "?: ");
	test(0, 0, 0);
	printf("%s", "?: ");
	test(0, 0, 1);

	test(-1, -1, 0);
	test(-1, 0, 1);
	test(-1, 1, -1);

	test(0, 1, 0);

	test(1, 1, 1);
	test(1, 2, 1);
	
	test(3, 0, 1);
	test(3, 1, 3);
   	test(3, 2, 9);	
	test(3, 3, 27);
	test(3, 4, 81);

	test(2, 2, 4);
	test(2, 3, 8);
	test(2, 4, 16);

	test(2, 15, 32768);
	test(2, 20, 1048576);
	test(2, 31, 2147483648);
	test(2, 32, 4294967296);
	test(2, 33, 8589934592);

}
